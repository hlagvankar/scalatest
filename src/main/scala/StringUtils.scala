package com.harshal.utils

object StringUtils {
        implicit class StringUtilities1(val s: List[String]) {
                def filterLines(f: String => Boolean): List[String] = {
                        s.filter(f).toList
                }

                def filterWords(f: String => Boolean): List[String] = {
                        val list = s.toString.split(" ")
                        list.filter(f).toList
                }
        }

        implicit class StringUtilities(val s: String) {
                def filterLines(f: String => Boolean): List[String] = {
                        val itr = s.toLowerCase.split("\n").toIterator
                        itr.filter(f).toList
                }

                def filterWords(f: String => Boolean): List[String] = {
                        val itr = s.split("\n").toIterator
                        itr.flatMap(_.split(" ")).toList.filter(f)
                }
        }
}
